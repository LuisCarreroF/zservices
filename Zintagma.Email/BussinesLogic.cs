﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SendGrid;
using SendGrid.Helpers.Mail;
using Zintagma.Entity.Email;

namespace ZintagmaServices.Email.DAL
{

        public class EmailBL 
        {
            private readonly IConfiguration _configuration;
            // Use our configuration to send the email by using SmtpClient

            public EmailBL(IConfiguration configuration)
            {
                _configuration = configuration;
            }
            public async Task SendEmailAsync(EmailRequest value)
            {
                var apiKey = _configuration["Email:key"];
                var client = new SendGridClient(apiKey);
                var from = new EmailAddress(value.FromEmail, value.FromText);
                var to = new EmailAddress(value.Email, value.Email);
                var plainTextContent = "No idea why I need send this";
                var htmlContent = value.HtmlMessage;
                var msg = MailHelper.CreateSingleEmail(from, to, value.Subject, plainTextContent, htmlContent);
                _ = await client.SendEmailAsync(msg);

                await Task.CompletedTask;
            }
        }
    }
