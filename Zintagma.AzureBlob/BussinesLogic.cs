﻿using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Zintagma.AzureBlob
{
    public class AzureBlobBL
    {
        CloudBlobContainer ConnectAzureStorage(string Connection, string ContainerBlob)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Connection);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference(ContainerBlob);

            return container;
        }
        public async Task<string> UploadBlobAsync(string Connection, string ContainerBlob, byte[] data, string name, string type)
        {
            CloudBlobContainer Container = ConnectAzureStorage(Connection , ContainerBlob);
            ImagenProcessor imageProcessor = new ImagenProcessor();
            var types = imageProcessor.GetMimeTypes();
            name += types[type];
            var newBlob = Container.GetBlockBlobReference(name);
            newBlob.Properties.ContentType = type;
            newBlob.Properties.CacheControl = "public, max-age=525600";
            await newBlob.UploadFromByteArrayAsync(data, 0, data.Length);

            return newBlob.Name;
        }
        public async Task<bool> DeleteBlobAsync(string Connection, string ContainerBlob, string id)
        {

            CloudBlobContainer Container = ConnectAzureStorage(Connection, ContainerBlob);

            var Blob = Container.GetBlockBlobReference(id);
            await Blob.DeleteIfExistsAsync();

            return true;
        }
    }
}
