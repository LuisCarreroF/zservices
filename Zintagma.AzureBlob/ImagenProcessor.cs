﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Metadata.Profiles.Exif;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

using SixLabors.Primitives;
using System;
using System.Collections.Generic;
using System.IO;
using Zintagma.Entity.AzureBlob;


namespace Zintagma.AzureBlob
{
    public class ImagenProcessor
    {
        #region Resize

        public MemoryStream ResizeImage(Stream Value, int width, int height, string ContentType)
        {
            string FileExtension = GetMimeTypes()[ContentType];
            Value.Seek(0, SeekOrigin.Begin);
            var ImageObj = Image.Load(Value);
            MemoryStream Result = new MemoryStream();

            var Options = new ResizeOptions()
            {
                Mode = (ResizeMode)Enum.Parse(typeof(ResizeMode), "0"),
                Size = new Size(width, height)
            };

            using (Image<Rgba32> destResize = (Image<Rgba32>)ImageObj.Clone(x => x.Resize(Options)))
            {
                destResize.Save(Result, Configuration.Default.ImageFormatsManager.FindFormatByFileExtension(FileExtension));
            }

            Result.Position = 0;

            return Result;
        }
        #endregion
        public Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {"text/plain",".txt"},
                {"application/pdf",".pdf"},
                {"application/vnd.ms-word",".docx" },
                {"application/vnd.ms-excel",".xls" },
                {"application/vnd.openxmlformats officedocument.spreadsheetml.sheet",".xlsx" },
                {"image/png",".png" },
                {"image/jpeg",".jpg"},
                {"image/gif",".gif" },
                {"text/csv",".csv"}
            };
        }
        public ImageSize GetSizes(Stream Value)
        {

            var imageObj = Image.Load(Value);
            ImageSize result = new ImageSize
            {
                Width = imageObj.Width,
                Height = imageObj.Height
            };
            return result;


        }
        public byte[] TransformImageIfNeeded(byte[] imageInBytes)
        {
            using (var image = Image.Load(imageInBytes))
            {
                ExifValue exifOrientation = image.Metadata?.ExifProfile?.GetValue(ExifTag.Orientation);

                if (exifOrientation == null) return imageInBytes;

                RotateMode rotateMode;
                FlipMode flipMode;
                SetRotateFlipMode(exifOrientation, out rotateMode, out flipMode);

                image.Mutate(x => x.RotateFlip(rotateMode, flipMode));
                image.Metadata.ExifProfile.SetValue(ExifTag.Orientation, (ushort)1);

                var imageFormat = Image.DetectFormat(imageInBytes);

                return ImageToByteArray(image, imageFormat);
            }
        }

        public byte[] ImageToByteArray(Image<Rgba32> image, IImageFormat imageFormat)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, imageFormat);
                return ms.ToArray();
            }
        }

        public void SetRotateFlipMode(ExifValue exifOrientation, out RotateMode rotateMode, out FlipMode flipMode)
        {
            var orientation = exifOrientation.Value.ToString();

            switch (orientation)
            {
                case "2":
                    rotateMode = RotateMode.None;
                    flipMode = FlipMode.Horizontal;
                    break;
                case "3":
                    rotateMode = RotateMode.Rotate180;
                    flipMode = FlipMode.None;
                    break;
                case "4":
                    rotateMode = RotateMode.Rotate180;
                    flipMode = FlipMode.Horizontal;
                    break;
                case "5":
                    rotateMode = RotateMode.Rotate90;
                    flipMode = FlipMode.Horizontal;
                    break;
                case "6":
                    rotateMode = RotateMode.Rotate90;
                    flipMode = FlipMode.None;
                    break;
                case "7":
                    rotateMode = RotateMode.Rotate90;
                    flipMode = FlipMode.Vertical;
                    break;
                case "8":
                    rotateMode = RotateMode.Rotate270;
                    flipMode = FlipMode.None;
                    break;
                default:
                    rotateMode = RotateMode.None;
                    flipMode = FlipMode.None;
                    break;
            }
        }
    }
}