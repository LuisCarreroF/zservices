﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace ZintagmaServices.DAL.Configuration
{
    public static class Configuration
    {
        #region Property
        private static IConfigurationRoot ConfigurationSite { get; set; }
        #endregion
        #region Configuration
        static Configuration()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", true, true).AddEnvironmentVariables();
            ConfigurationSite = builder.Build();
        }
        #endregion
        #region GetValue
        public static string GetValue(string key)
        {
            return ConfigurationSite.GetSection(key).Value;
        }
        #endregion
    }
}
