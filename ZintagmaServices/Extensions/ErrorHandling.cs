﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace ZintagmaServices.Extensions
{
    public class ErrorHandling
    {
        public static ActionResult Result(HttpStatusCode statusCode, string reason) => new ContentResult
        {
            StatusCode = (int)statusCode,
            Content = $"Status Code: {(int)statusCode}; {statusCode}; {reason}",
            ContentType = "text/plain",
        };
    }
}
