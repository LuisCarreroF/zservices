﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ZintagmaServices.DAL.Configuration;

namespace ZintagmaServices
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class Security : ActionFilterAttribute
    {
        #region Contructor

        #endregion
        #region Override
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!string.IsNullOrEmpty(context.HttpContext.Request.Headers["ZintagmaToken"]) && context.HttpContext.Request.Headers["ZintagmaToken"] == DAL.Configuration.Configuration.GetValue("Configuration:SecurityToken"))
            {
               
            }
            else
            {
                context.Result = new UnauthorizedResult();
            }

        }
        #endregion


    }

}
