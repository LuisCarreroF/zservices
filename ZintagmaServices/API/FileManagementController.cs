﻿using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using Zintagma.Excel;
using Swashbuckle.AspNetCore.Annotations;
using Zintagma.Entities.Entities.Excel;
using Zintagma.Entity.AzureBlob;

namespace ZintagmaServices.API
{
    [ApiController]
    [Route("[controller]")]
    public class FileManagementController : ControllerBase
    {
        private readonly ExcelBL _excelBL;
        public FileManagementController(ExcelBL excelBL)
        {
            _excelBL = excelBL;
        }





        [HttpPost("JsonToExcel")]
        [Security]
        [SwaggerOperation(Summary = "JsonToExcel", Description = "convert a json to excel")]
        public IActionResult ExportToExcel(dynamic value)
        {
            CustomFile result = _excelBL.ExportExcel(value);
            return this.File(result.FileContents, result.ContentType, result.FileName);
        }







        [HttpPost("ExcelToJson")]
        [Security]
        [SwaggerOperation(Summary = "ExcelToJson", Description = "convert a excel to json")]
        public IActionResult ImportToExcel([FromForm] FileRequest value)
        {
            string result = _excelBL.ImportExcel(value.File);
            return Ok(JsonConvert.DeserializeObject(result));
        }
        
    }

}
