﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Zintagma.Entity.Helper;
using Zintagma.Helper;
using ZintagmaServices.Extensions;

namespace ZintagmaServices.API
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class HelperController : ControllerBase
    {
        private readonly EncryptHandelerBL _helperService;
        public HelperController(EncryptHandelerBL helperService)
        {
            _helperService = helperService;
        }
        [HttpPost("Encrypt")]
        [Security]
        [SwaggerOperation(Summary = "Encrypt", Description = "Encrypt using key, return string with text encrypt")]
        public IActionResult Encrypt([FromBody]Crypt value)
        {
            if (value.text == null || value.key == null)
                return ErrorHandling.Result(HttpStatusCode.BadRequest, "Missed parameters.");

            var text = _helperService.Encrypt(value.text, value.key);

            return Ok(text);
        }
        [HttpPost("Decrypt")]
        [Security]
        [SwaggerOperation(Summary = "Decrypt", Description = "Decrypt using key, return string with text decrypt")]
        public IActionResult Decrypt([FromBody]Crypt value)
        {
            if (value.text == null || value.key == null)
                return ErrorHandling.Result(HttpStatusCode.BadRequest, "Missed parameters.");

            var text = _helperService.Decrypt(value.text, value.key);

            return Ok(text);
        }
    }
}