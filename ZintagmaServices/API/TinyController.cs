﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Zintagma.Entity.Tiny;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Zintagma.Tiny;
using ZintagmaServices.Extensions;

namespace ZintagmaServices.API
{
    [Route("[controller]")]
    [ApiController]
    public class TinyController : ControllerBase
    {
        private readonly TinyBL _tinyBL;
        public TinyController(TinyBL tinyBL)
        {
            _tinyBL = tinyBL;
        }

        [HttpPost("CompressImage")]
        [Security]
        [SwaggerOperation(Summary = "CompressImage", Description = "Upload  a file in AzureBlob, if file is jpg or png an image avatar is created")]
        public async Task<IActionResult> CompressImagen([FromForm]TinyRequest value)
        {
            if (value == null)
            {
                return ErrorHandling.Result(HttpStatusCode.BadRequest, "Missed parameters.");
            }
            using (var ms = new MemoryStream())
            {
                value.File.CopyTo(ms);
                if (value.File.ContentType == "image/jpeg" || value.File.ContentType == "image/png")
                {
                    byte[] data = ms.ToArray();
                    var bytesImages = await _tinyBL.CompressImageAsync("ZHrdCYcWwC4SWJm9fZ45s59jzY1kV6vY", data);
                    Stream stream = new MemoryStream(bytesImages);
                    return File(stream, value.File.ContentType);
                }
                else
                {
                    return ErrorHandling.Result(HttpStatusCode.BadRequest, "The file have to extention .jpg or .png");
                }
            }
            
        }
    }
}