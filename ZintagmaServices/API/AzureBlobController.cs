﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Zintagma.AzureBlob;
using Zintagma.Entity.AzureBlob;
using ZintagmaServices.Extensions;

namespace ZintagmaServices.API
{
    //[Produces("form-data")]
    [ApiController]
    [Route("[controller]")]
    public class AzureBlobController : ControllerBase
    {
        private readonly AzureBlobBL _azureBlobBL;

        public AzureBlobController(AzureBlobBL azureBlobBL) {
            _azureBlobBL = azureBlobBL;
        }

        [HttpPost("UpLoadBlob")]
        [Security]
        [SwaggerOperation(Summary = "UpLoad", Description = "Upload  a file in AzureBlob, if file is jpg or png an image avatar is created")]
        public async Task<IActionResult> UpLoadBlobAsync([FromForm]UpLoadBlobRequest value)
        {
            ImagenProcessor image = new ImagenProcessor();
            if (value.Connection == null || value.ContainerBlob == null || value.File == null || value.Name == null || value.Type == null)
                return ErrorHandling.Result(HttpStatusCode.BadRequest, "Missed parameters.");
            using (var ms = new MemoryStream())
            {
                value.File.CopyTo(ms);
                byte[] rotate = ms.ToArray();
                string newname = Guid.NewGuid().ToString();
                if (value.File.ContentType == "image/jpeg" || value.File.ContentType == "image/png")
                {
                    rotate = image.TransformImageIfNeeded(ms.ToArray());
                    ms.Position = 0;
                    if (value.Width == 0)
                    {
                        value.Width = 50;
                    }
                    if (value.Height == 0)
                    {
                        value.Height = 50;
                    }
                    var data = image.ResizeImage(ms, value.Width, value.Height, value.File.ContentType).ToArray();
                    await _azureBlobBL.UploadBlobAsync(value.Connection, value.ContainerBlob, data, "avatar_" + newname, value.File.ContentType);
                    
                }
                
                var result = await _azureBlobBL.UploadBlobAsync(value.Connection, value.ContainerBlob, rotate, newname, value.File.ContentType);
                if(value.File.ContentType == "image/jpeg" || value.File.ContentType == "image/png")
                {
                    return Ok(result + ", avatar_" + result);
                }
                return Ok(result);

            }
        }

    }
}