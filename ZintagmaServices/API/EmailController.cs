﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using ZintagmaServices.Email.DAL;
using Zintagma.Entity.Email;
using ZintagmaServices.Extensions;

namespace ZintagmaServices.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class EmailController : ControllerBase
    {
        #region Contructor
        private readonly EmailBL _emailService;
        public EmailController(EmailBL emailService)
        {
            _emailService = emailService;
        }
        #endregion

        [HttpPost("SendEmail")]
        [Security]
        [SwaggerOperation(Summary = "Send Email", Description = "Send Email using SendGrid")]
        public async Task<IActionResult> SendEmail([FromBody]EmailRequest value)
        {
            if (value.Email == null || value.Subject == null || value.HtmlMessage == null)
                return ErrorHandling.Result(HttpStatusCode.BadRequest, "Missed parameters.");

            await _emailService.SendEmailAsync(value);

            return Ok();
        }
    }
}