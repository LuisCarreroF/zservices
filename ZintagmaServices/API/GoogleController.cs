﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using ZintagmaServices.Extensions;
using Zintagma.Google;
using Zintagma.Entity.Google;

namespace ZintagmaServices.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class GoogleController : ControllerBase
    {
        [HttpPost("GetGeolocation")]
        [Security]
        [SwaggerOperation(Summary = "Calculate Geolocation for an address", Description = "Give an adress with country and city will return a Latitude and Longitude values, in case don't found a match will return null")]
        public async Task<IActionResult> GetGeolocation([FromBody]GeolocationRequest value)
        {
            BussinesLogic BL = new BussinesLogic();
            if (value.Address == null || value.API == null)
                return ErrorHandling.Result(HttpStatusCode.BadRequest, "Missed parameters.  ");

            var result = await BL.CalculateGeolocation(value);


            if (result == null)
                return ErrorHandling.Result(HttpStatusCode.NotFound, "Address not Found");

            else
                return Ok(result);
        }

        [HttpPost("GetRoutes")]
        [Security]
        [SwaggerOperation(Summary = "Calculate the best route between 2  addresses", Description = "Provide 2 addresses and will return an array with the optimized route")]
        public async Task<IActionResult> GetRoute([FromBody]RouteRequest value)
        {
            _ = new RoutesResult();
            BussinesLogic BL = new BussinesLogic();
            RoutesResult result;
            try
            {
                result = await BL.CalculateRoute(value);
            }
            catch (Exception ex)
            {
                return ErrorHandling.Result(HttpStatusCode.BadGateway, ex.Message);
            }
            return Ok(result);
        }
    }
}