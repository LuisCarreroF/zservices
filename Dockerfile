FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80

ENV ASPNETCORE_URLS=http://*:80
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["/ZintagmaServices/ZintagmaServices.csproj", "./"]
RUN dotnet restore "ZintagmaServices.csproj"
COPY . .
WORKDIR "/src/ZintagmaServices"
RUN dotnet build "ZintagmaServices.csproj" -c Release -o /app/build

FROM build AS publish
WORKDIR "/src/ZintagmaServices"
RUN dotnet publish "ZintagmaServices.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ZintagmaServices.dll"]
