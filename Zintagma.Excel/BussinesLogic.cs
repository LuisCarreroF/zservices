﻿using ClosedXML.Excel;
using ExcelDataReader;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Zintagma.Entities.Entities.Excel;

namespace Zintagma.Excel
{
    public class ExcelBL
    {
        public ExcelBL()
        {

        }
        public CustomFile ExportExcel(dynamic value) 
        {
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Sheet");
                var currentRow = 1;

                string json = JsonConvert.SerializeObject(value);
                JArray array = JArray.Parse(json);
                foreach (JObject content in array.Children<JObject>())
                {
                    var cont = 1;
                    foreach (JProperty prop in content.Properties())
                    {
                        if (currentRow == 1)
                        {
                            worksheet.Cell(currentRow, cont).Value = prop.Name;
                            worksheet.Cell(currentRow + 1, cont).Value = prop.Value.ToString();
                        }
                        else if (currentRow == 2)
                        {
                            currentRow++;
                            worksheet.Cell(currentRow, cont).Value = prop.Value.ToString();
                        }
                        else
                        {
                            worksheet.Cell(currentRow, cont).Value = prop.Value.ToString();
                        }
                        cont++;

                    }
                    cont = 1;
                    currentRow++;
                }
                using (MemoryStream stream = new MemoryStream())
                {
                    workbook.SaveAs(stream);
                    var content = stream.ToArray();
                    CustomFile customFile = new CustomFile();
                    customFile.FileContents = stream.ToArray();
                    customFile.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    customFile.FileName = "Document.xlsx";
                    return customFile;
                }



            }
        }

        public string ImportExcel(IFormFile value)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                value.CopyTo(ms);
                var excelContent = ParseExcel(new MemoryStream(ms.ToArray()));
                return JsonConvert.SerializeObject(excelContent);
            }
        }

        private static IEnumerable<Dictionary<string, object>> ParseExcel(Stream document)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var reader = ExcelReaderFactory.CreateReader(document))
            {
                var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    UseColumnDataType = true,
                    ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true,
                    }
                });
                return MapDatasetData(result.Tables.Cast<DataTable>().First());
            }
        }
        private static IEnumerable<Dictionary<string, object>> MapDatasetData(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                var row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                yield return row;
            }
        }
    }
}
