﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Zintagma.Entity.Google;

namespace Zintagma.Google.DAL
{
    class Conection
    {
        private string _apiKey;
        private string _baseURL= $"https://maps.googleapis.com/maps/api/";
        public Conection(string apikey)
        {
            _apiKey = apikey;
        }
        static readonly HttpClient httpClient = new HttpClient();
        public async Task<string> Connect(string module, string parameters)
        {
            string executeURL = _baseURL + module + parameters+ $"&key={_apiKey}";

            string result = await httpClient.GetStringAsync(executeURL);
            var json = JsonConvert.DeserializeObject<Response>(result);

            if (json.status == "OK" || json.status == "ZERO_RESULTS")
                return result;
            else
                throw new Exception(json.status);
        }
    }
}