﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Zintagma.Entity.Google;
using Zintagma.Google.DAL;

namespace Zintagma.Google
{
    public class BussinesLogic
    {
        #region Methods
        public async Task<LocationResponse> CalculateGeolocation(GeolocationRequest value)
        {
            Conection _conection = new Conection(value.API);
            value.Address = PreClean(value.Address);
            Search searchResult;

            searchResult = await Search(value.Address, value.API);

            if (searchResult.Candidates.Count > 0)
            {
                LocationResponse result = new LocationResponse();
                var response = await Details(searchResult.Candidates[0].place_id,value.API);
                result.Address = response.Result.address_components[0].long_name;
                result.PlaceID = searchResult.Candidates[0].place_id;
                result.Latitude = response.Result.Geometry.Location.lat;
                result.Longitude = response.Result.Geometry.Location.lng;
                return result;
            }
            return null;
        }
        public async Task<RoutesResult> CalculateRoute(RouteRequest value)
        {
            Conection _conection = new Conection(value.API);
            RoutesResult result = new RoutesResult();
            _ = new RoutesResponse();
            string route = "";
            string route2 = PreClean(value.Origin)+"/";
            string Origin = PreClean(value.Origin);
            string Destination = PreClean(value.Destination);
            string wayPoints = "";

            foreach (var local in value.Steps)
            {
                wayPoints += PreClean(local) + "|";
            }
            string URL = $"json?origin={Origin}&destination={Destination}&waypoints=optimize:true|{wayPoints}";

            RoutesResponse response = JsonConvert.DeserializeObject<RoutesResponse>(await _conection.Connect("directions/",URL));
            var first = response.routes[0].legs.First();
            var last = response.routes[0].legs.Last();
            foreach (var local in response.routes[0].legs)
            {

                URL = $"json?origin={PreClean(local.start_address)}&destination={PreClean(local.end_address)}&departure_time={value.StartTimeUTC}&duration_in_traffic={value.StartTimeUTC}&traffic_model=pessimistic";
                RoutesResponse pointPoint = JsonConvert.DeserializeObject<RoutesResponse>(await _conection.Connect("directions/", URL));

                var durationTraffic = "";
                var durationTrafficValue = 0;
                foreach (var localPointPoint in pointPoint.routes[0].legs)
                {
                    durationTraffic = localPointPoint.duration_in_traffic.text;
                    durationTrafficValue = localPointPoint.duration_in_traffic.value;
                }
                value.StartTimeUTC = (Convert.ToDouble(value.StartTimeUTC) + ConvertDurationtoMinUTC(durationTraffic)).ToString();
                result.Steps.Add(new RoutesSteps
                {
                    Address = local.end_address,
                    DistanceValue = local.distance.value,
                    DurationValue = local.duration.value,
                    DurationTraffic = durationTraffic,
                    DurationTrafficvalue = durationTrafficValue,
                    ArrivelTime = value.StartTimeUTC,
                    Distance = local.distance.text,
                    Duration = local.duration.text,
                    Longitude = local.end_location.lng,
                    Latitud = local.end_location.lat,
                    WazeLink = $"https://waze.com/ul?ll={local.end_location.lat},{local.end_location.lng}&navigate=yes"
                });
                if (local.Equals(first))
                {
                    route += "&daddr=" + PostClean(local.end_address);
                    result.Origin = new locationGeneric
                    {
                        lat = local.start_location.lat,
                        lng = local.start_location.lng
                    };
                }
                else
                    route += "+to:" + PostClean(local.end_address);

                if (local.Equals(last))
                {
                    result.Destination = new locationGeneric { 
                        lat = local.end_location.lat,
                        lng = local.end_location.lng
                    };
                }

                route2 += PostClean(local.end_address) + "/";
            }

            Origin = PostClean(value.Origin);

            if(result.Steps.Count()<=10)
            result.GoogleURL = $"http://maps.google.com/maps?f=d&saddr={Origin}{route}";
            else
            result.GoogleURL = $"https://www.google.com/maps/dir/{route2}";

            return result;
        }
        #endregion

        #region Support Methods
        private async Task<Details> Details(string place_id, string API)
        {
            Conection _conection = new Conection(API);
            string URL = $"json?place_id={place_id}";
            string json = await _conection.Connect("place/details/", URL);
            var result = JsonConvert.DeserializeObject<Details>(json);
            return result;
        }
        private async Task<Search> Search(string value, string API)
        {
            Conection _conection = new Conection(API);
            string URL = $"json?input={value}&inputtype=textquery&key={API}";
            string result = await _conection.Connect("place/findplacefromtext/", URL);
            return JsonConvert.DeserializeObject<Search>(result);
        }
        public string PreClean(string value)
        {
            return value.Replace("#", " ").Replace(" ", "+");
        }
        public string PostClean(string value)
        {

            return value.Replace(" ", "+").Replace("#", "").Replace("&", "");
        }
        public double ConvertDurationtoMinUTC(string duration) {
            double minutes = 0;
            var split = duration.Split(" ");
            var test = split.ElementAt(0);
            if (split.Count() == 4)
            {
                minutes = Convert.ToDouble(split.ElementAt(0)) * 3600000;
                minutes += Convert.ToDouble(split.ElementAt(2)) * 60000;
            }
            if (split.Count() == 2)
            {
                minutes = Convert.ToDouble(split.ElementAt(0)) * 60000;
            }

            return minutes;
        }
        #endregion
    }
}
