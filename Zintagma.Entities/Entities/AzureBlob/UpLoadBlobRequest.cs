﻿using Microsoft.AspNetCore.Http;

namespace Zintagma.Entity.AzureBlob
{
    public class UpLoadBlobRequest
    {
        public string Connection { get; set; }
        public string ContainerBlob { get; set; }
        public IFormFile File { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        
    }
}
