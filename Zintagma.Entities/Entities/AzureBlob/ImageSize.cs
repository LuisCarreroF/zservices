﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zintagma.Entity.AzureBlob
{
    public class ImageSize
    {
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
