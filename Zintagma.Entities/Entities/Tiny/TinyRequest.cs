﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Zintagma.Entity.Tiny
{
    public class TinyRequest
    {
        public IFormFile File { get; set; }
    }
}
