﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zintagma.Entities.Entities.Excel
{
    public class JsonRequest
    {
        public dynamic Json { get; set; }
        public string NameFile { get; set; }
        public string NameSheet { get; set; }

    }
}
