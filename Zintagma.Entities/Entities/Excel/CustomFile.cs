﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zintagma.Entities.Entities.Excel
{
    public class CustomFile
    {
        public byte[] FileContents { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }
}
