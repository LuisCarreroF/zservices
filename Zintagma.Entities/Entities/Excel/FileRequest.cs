﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Zintagma.Entities.Entities.Excel
{
    public class FileRequest
    {
        public string Type { get; set; }

        public IFormFile File { get; set; }
        public string Name { get; set; }
    }
}
