﻿using System;
namespace Zintagma.Entity.Email
{
    public class EmailRequest
    {
        public string Email { get; set; }
        public string Subject { get; set; }
        public string HtmlMessage { get; set; }
        public string Name { get; set; }
        public string FromEmail { get; set; }
        public string FromText { get; set; }
    }
}