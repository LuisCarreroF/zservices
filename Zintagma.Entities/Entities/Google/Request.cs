﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Zintagma.Entity.Google
{
    public class GeolocationRequest
    {
        public string API { get; set; }
        public string Address { get; set; }
    }
    public class RouteRequest
    {
        public string API { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public List<string> Steps { get; set; }
        public string StartTimeUTC { get; set; }
    }
    public class LocationResponse
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string PlaceID { get; set; }
        public string Address { get; set; }
    }
    public class Base<T> where T : Response
    {
    }
    public class Response 
    {
        [JsonProperty("status")]
        public string status { get; set; }
    }
}
