﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Zintagma.Entity.Google
{
    public class Details
    {
        [JsonProperty("result")]
        public Result Result { get; set; }
    }
    public class Result
    {
        [JsonProperty("formatted_address")]
        public string formatted_address { get; set; }

        [JsonProperty("address_components")]
        public List<Address_Components> address_components { get; set; }

        [JsonProperty("geometry")]
        public Geometry Geometry { get; set; }
    }
    public class Geometry
    {
        [JsonProperty("location")]
        public Location Location { get; set; }

    }
    public class Location
    {
        [JsonProperty("lat")]
        public double lat { get; set; }
        [JsonProperty("lng")]
        public double lng { get; set; }
    }
    public class Address_Components
    {
        [JsonProperty("long_name")]
        public string long_name { get; set; }
    }
}
