﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Zintagma.Entity.Google
{

    public class RoutesResponse
    {

        [JsonProperty("geocoded_waypoints")]
        public List<geocoded_waypoints> geocoded_waypoints { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
        [JsonProperty("routes")]
        public List<routes> routes { get; set; }
    }

    public class routes
    {
        [JsonProperty("legs")]
        public List<legs> legs { get; set; }

    }
    public class geocoded_waypoints
    {
        [JsonProperty("place_id")]
        public string place_id { get; set; }
        [JsonProperty("geocoder_status")]
        public string geocoder_status { get; set; }
    }
    public class legs
    {
        [JsonProperty("distance")]
        public distance distance { get; set; }
        [JsonProperty("duration")]
        public duration duration { get; set; }
        [JsonProperty("steps")]
        public List<steps> steps { get; set; }
        [JsonProperty("end_address")]
        public string end_address { get; set; }
        [JsonProperty("start_address")]
        public string start_address { get; set; }
        [JsonProperty("end_location")]
        public location end_location { get; set; }
        [JsonProperty("start_location")]
        public location start_location { get; set; }
        [JsonProperty("duration_in_traffic")]
        public durationTraffic duration_in_traffic { get; set; }

    }
    public class distance
    {
        [JsonProperty("text")]
        public string text { get; set; }
        [JsonProperty("value")]
        public int value { get; set; }

    }
    public class duration
    {
        [JsonProperty("text")]
        public string text { get; set; }
        [JsonProperty("value")]
        public int value { get; set; }

    }
    public class steps
    {
        [JsonProperty("distance")]
        public distance distance { get; set; }
        [JsonProperty("duration")]
        public duration duration { get; set; }
        [JsonProperty("end_location")]
        public location end_location { get; set; }
        [JsonProperty("start_location")]
        public location start_location { get; set; }


    }
    public class location
    {
        [JsonProperty("lat")]
        public double lat { get; set; }
        [JsonProperty("lng")]
        public double lng { get; set; }

    }
    public class locationGeneric
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class RoutesResult
    {
        public List<RoutesSteps> Steps { get; set; } = new List<RoutesSteps>();
        public string GoogleURL { get; set; }
        public locationGeneric Origin { get; set; }
        public locationGeneric Destination { get; set; }
    }
    public class RoutesSteps
    {
        public string Address { get; set; }
        public string Duration { get; set; }
        public string DurationTraffic { get; set; }
        public string ArrivelTime { get; set; }
        public int DurationTrafficvalue { get; set; }
        public string Distance { get; set; }
        public int DurationValue { get; set; }
        public int DistanceValue { get; set; }
        public string WazeLink { get; set; }
        public double Latitud { get; set; }
        public double Longitude { get; set; }
    }
    public class durationTraffic {
        [JsonProperty("text")]
        public string text { get; set; }
        [JsonProperty("value")]
        public int value { get; set; }
    }

}

