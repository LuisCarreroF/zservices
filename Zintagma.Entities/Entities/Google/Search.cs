﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Zintagma.Entity.Google
{
    public class Search
    {
        [JsonProperty("candidates")]
        public List<Candidates> Candidates { get; set; }
        [JsonProperty("error_messages")]
        public string error_messages { get; set; }
        [JsonProperty("status")]
        public string status { get; set; }
    }
    public class Candidates
    {
        [JsonProperty("place_id")]
        public string place_id { get; set; }
    }
}