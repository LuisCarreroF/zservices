﻿using System.Threading.Tasks;
using TinifyAPI;

namespace Zintagma.Tiny
{
    public class TinyBL
    {
        void Conection(string APIKey){
            Tinify.Key = APIKey;
        }
        public async Task<byte[]> CompressImageAsync(string APIKey, byte[] data) {
            Conection(APIKey);
            return await Tinify.FromBuffer(data).ToBuffer();

        }
    }
}
